import React, { useState } from "react";
import AppHeader from "./components/appHeader/appHeader.js";
import todoData from "./components/todoData";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import SearchPanel from "./components/searchPanel/searchPanel.js";
import FilterPanel from "./components/filterPanel/filterPanel.js";
import TodoList from "./components/todoList/todoList.js";
import TodoAdd from "./components/todoAdd/todoAddForm.js";

let maxId = 100;
const createTodoList = (label) => {
  return {
    label,
    completed: false,
    important: false,
    id: maxId++,
  };
};
const initialTodos = [
  createTodoList("Drink Coffee"),
  createTodoList("Make Awesome App"),
  createTodoList("Have a lunch"),
];

const App = () => {
  const [todos, setTodos] = useState(initialTodos);
  const [completedTodo, setCompletedTodo] = useState([]);
  const [activeTodo, setActiveTodo] = useState([]);
  const [activeCounter, setActiveCounter] = useState(0);
  const addTodo = (label) => {
    const newTodo = createTodoList(label);
    setTodos([...todos, newTodo]);
  };

  const onToggleDone = (todoID) => {
    const toggleTodo = todos.find((item) => item.id === todoID);
    toggleTodo.completed = !toggleTodo.completed;
    const newTodos = todos.filter((item) => item.id !== todoID);
    const activeTodoArr = todos.filter((todo) => todo.completed === false);
    setTodos([...newTodos, toggleTodo]);
    setCompletedTodo([...completedTodo, toggleTodo]);
    setActiveCounter(activeTodoArr.length);
    setActiveTodo(activeTodoArr);
    console.log("completed=>" + activeTodo);
  };

  function onDelete(todoID) {
    const newTodos = todos.filter((item) => item.id !== todoID);
    setTodos(newTodos);
  }
  function onImportant(todoID) {
    const importantTodo = todos.find((item) => item.id === todoID);
    importantTodo.important = !importantTodo.important;
    const newTodos = todos.filter((item) => item.id !== todoID);
    setTodos([...newTodos, importantTodo]);
  }
  function allFilter() {
    // const newTodos = todos.filter((todo) => todo);
    setTodos(completedTodo.concat(activeTodo));
  }
  function completedFilter() {
    setTodos(completedTodo);
  }
  function activeFilter() {
    setTodos(activeTodo);
  }
  return (
    <div className="App w-75 mx-auto">
      <AppHeader todos={todos} activeCounter={activeCounter} />
      <div className="search-filter-panel w-75">
        <SearchPanel />
        <FilterPanel
          allFilter={allFilter}
          completedFilter={completedFilter}
          activeFilter={activeFilter}
        />
      </div>
      <TodoList
        todos={todos}
        onToggleDone={onToggleDone}
        onDelete={onDelete}
        onImportant={onImportant}
      />
      <TodoAdd addTodo={addTodo} />
    </div>
  );
};

export default App;
