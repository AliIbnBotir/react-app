const todoData = () => {
  return [
    {
      text: "Drink Coffee",
      completed: false,
      important: false,
      id: 1,
    },
    {
      text: "Make Awesome App",
      completed: false,
      important: false,
      id: 2,
    },
    {
      text: "have a lunch",
      completed: false,
      important: false,
      id: 3,
    },
  ];
};

export default todoData;
