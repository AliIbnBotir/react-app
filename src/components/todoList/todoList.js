import React from "react";
import "./todoList.css";
import TodoListItem from "../todoListItem/todoListItem.js";

const TodoList = ({ todos, onToggleDone, onDelete, onImportant }) => {
  return (
    <div className="todo-list  rounded w-75">
      <ul>
        {todos.map((todo) => (
          <li key={todo.id} className="list-item border">
            <TodoListItem
              {...todo}
              onToggleDone={onToggleDone}
              onDelete={onDelete}
              onImportant={onImportant}
            />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default TodoList;
