import React from "react";
import "./todoListItem.css";
import { BsTrash, BsFillExclamationSquareFill } from "react-icons/bs";

const TodoListItem = ({
  label,
  onToggleDone,
  id,
  completed,
  important,
  onDelete,
  onImportant,
}) => {
  return (
    <span className="todo-list-item w-100">
      <span
        className="todo-list-item__label"
        style={{
          textDecoration: completed ? "line-through" : "none",
          fontWeight: important ? "bold" : "normal",
        }}
        onClick={() => {
          onToggleDone(id);
        }}
      >
        {label}
      </span>
      <span>
        <BsTrash onClick={() => onDelete(id)} className="icon trash-icon" />
        <BsFillExclamationSquareFill
          onClick={() => onImportant(id)}
          className="icon exclam-icon"
        />
      </span>
    </span>
  );
};

export default TodoListItem;
