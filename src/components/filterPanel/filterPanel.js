import React from "react";
import "./filterPanel.css";

const FilterPanel = ({ allFilter, completedFilter, activeFilter }) => {
  return (
    <div className="filter-panel w-50  d-flex rounded mx-1">
      <button onClick={() => allFilter()} className="btn-all active">
        All
      </button>
      <button onClick={() => completedFilter()} className="btn-Completed">
        Completed
      </button>
      <button onClick={() => activeFilter()} className="btn-Closed">
        Active
      </button>
    </div>
  );
};

export default FilterPanel;
