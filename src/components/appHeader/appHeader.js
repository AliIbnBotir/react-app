import React, { useState } from "react";
import "./appHeader.css";
const AppHeader = ({ todos, activeCounter }) => {
  return (
    <div className="app-header mt-5">
      <h1 className="m-0">ToDo List</h1>
      <p className="m-0 mx-5 text-muted">
        <span>{activeCounter} </span> more to do,
        <span>{todos.length - activeCounter}</span> done
      </p>
    </div>
  );
};

export default AppHeader;
