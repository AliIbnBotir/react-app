import React, { useState } from "react";
import "./todoAdd.css";

const TodoAdd = ({ addTodo }) => {
  const [todo, setTodo] = useState("");
  return (
    <div className="todo-add d-flex w-75 ">
      <input
        type="text"
        placeholder="What needs to be done"
        className="rounded w-75 px-3"
        value={todo}
        onChange={(e) => {
          setTodo(e.target.value);
        }}
      />
      <button
        onClick={() => {
          addTodo(todo);
          setTodo("");
        }}
        className="add-btn btn-sm rounded"
      >
        Add item
      </button>
    </div>
  );
};

export default TodoAdd;
