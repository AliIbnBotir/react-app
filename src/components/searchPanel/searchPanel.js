import React from "react";
import "./search-panel.css";
const SearchPanel = () => {
  return (
    <div className="search-panel m-5 mx-auto w-75">
      <input
        className="search-input w-100  mx-auto px-3"
        type="text"
        placeholder="type to search"
      />
    </div>
  );
};

export default SearchPanel;
